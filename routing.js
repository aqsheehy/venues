var simpleLoad = require('./simpleLoad');
var cqrs = require('./cqrs');

// Load handlers.
var venues = require('./handlers/venues.js');
var events = require('./handlers/events.js');
var persons = require('./handlers/persons.js');

module.exports = function(app){	
	simpleLoad(app, function(get, post, patch, put){

		get['/'] = function(req, res) { 
			res.render('index', { title: 'Home' });
		};

		post['/act'] = function(req, res){
			var c = new acts.ActRegistered();
			res.send(cqrs.send(c));
		};

 		get['/act/:id'] = function(req, res){
			var c = new acts.GetAct();
			res.send(cqrs.send(c));			
 		};

 		patch['/act/:id'] = function(req, res){
 			var c = new acts.ActUpdated();
 			res.send(cqrs.send(c));
 		}

		post['/act/:id/performance'] = function(req, res){
			var c = new acts.PerformingAt();
			res.send(cqrs.send(c));
		};

		get['/performance/:id'] = function(req, res){
			var c = new acts.GetPerformance();
			res.send(cqrs.send(c));
		};

		get['/event/:id'] = function(req, res) {
			var c = new events.GetEvent();
			res.send(cqrs.send(c));
		};

		post['/event/:id/attended'] = function(req, res){
			var c = new events.EventAttended();
			res.send(cqrs.send(c));
		};

		post['/event/:id/purchase'] = function(req, res){
			var c = new events.TicketPurchased();
			res.send(cqrs.send(c));
		};

		patch['/event/:id'] = function(req, res){
			var c = new events.EventUpdated();
			res.send(cqrs.send(c)); 
		};

		get['/person/:id'] = function(req, res){
			var c = new persons.GetPerson();
			res.send(cqrs.send(c));
		};

	});
};