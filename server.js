var port = process.env.PORT || 8008;

var express = require('express');
var bodyParser = require('body-parser');

// Initiate express.
var app = express();

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/app'));
app.use('/bower_components', express.static(__dirname + '/bower_components'));

// Load api routing.
require('./routing/api.js')(app);
require('./routing/app.js')(app);
require('./routing/facebook.js')(app);

app.listen(port);