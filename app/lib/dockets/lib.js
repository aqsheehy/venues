var Lib = angular.module('vns.dockets', []);

Lib.factory('Dockets', ['$q', '$http', function($q, $http){

	var Dockets = {};

	Dockets.host = 'infinite-headland-4046.herokuapp.com';
	Dockets.appHost = 'apps.facebook.com/bitdocket';

	Dockets.getEvent = function(eventId){
		var deferred = $q.defer();

		$http
			.get('/v1/event/' + eventId)
			.success(function(Event){ deferred.resolve(Event); });

		return deferred.promise;		
	};

	Dockets.getEventProductUrl = function(eventId){
		return 'http://' + Dockets.host + '/opengraph/event/' + eventId;
	};

	Dockets.getEventVenue = function(eventId){
		var deferred = $q.defer();

		$http
			.get('/v1/event/' + eventId + '/venue')
			.success(function(Venue){ deferred.resolve(Venue); });

		return deferred.promise;		
	};

	Dockets.getEventActs = function(eventId){
		var deferred = $q.defer();

		$http
			.get('/v1/event/' + eventId + '/acts')
			.success(function(Acts){ deferred.resolve(Acts); });

		return deferred.promise;		
	};

	Dockets.getEventTicket = function(eventId, userId){

		var deferred = $q.defer();

		$http
			.get('/v1/event/' + eventId + '/ticket?purchaser=' + userId)
			.success(function(TicketForEvent){ deferred.resolve(TicketForEvent); });

		return deferred.promise;

	};

	Dockets.getVenueEvents = function(venueId) {

		var deferred = $q.defer();

		$http
			.get('/v1/venue/' + venueId + '/event')
			.success(function(VenueEvents){ deferred.resolve(VenueEvents); });

		return deferred.promise;
	};

	Dockets.getVenue = function(venueId) {

		var deferred = $q.defer();

		$http
			.get('/v1/venue/' + venueId)
			.success(function(Venue){ deferred.resolve(Venue); });

		return deferred.promise;
	};

	Dockets.venueHosts = function(venueId, event){

		var deferred = $q.defer();

		$http
			.post('/v1/venue/' + venueId + '/event', {
				eventfbid: event.fbid,
				title: event.title,
				description: event.description,
				start: event.start,
				end: event.end,
				pricing: event.pricing
			})
			.success(function(){
				deferred.resolve();
			})

		return deferred.promise;

	};

	Dockets.registerAct = function(act){

		var deferred = $q.defer();

		act.fbid = act.fbid + '';
		act.description = act.description || '';

		$http
			.post('/v1/act/fb', act)
			.success(function(){ deferred.resolve(); });

		return deferred.promise;

	};

	Dockets.performingAt = function(actId, event){

		var deferred = $q.defer();

		event.fbid = event.fbid + '';

		$http.post('/v1/act/' + actId + '/performance', {
			eventfbid: event.fbid
		})
		.success(function(){ deferred.resolve(); });

		return deferred.promise;

	};

	Dockets.getTicket = function(ticketId)  {
		var deferred = $q.defer();

		$http
			.get('/v1/ticket/' + ticketId)
			.success(function(Ticket){ deferred.resolve(Ticket); });

		return deferred.promise;
	};

	Dockets.getTicketAttendanceUrl = function(Ticket){
		return 'https://' + Dockets.host + '/manage/ticket/' + Ticket.ticketId + '/attend?ticketSecret=' + Ticket.ticketSecret;
	};

	Dockets.getTicketEvent = function(ticketId) {
		var deferred = $q.defer();

		$http
			.get('/v1/ticket/' + ticketId + '/event')
			.success(function(Event){ deferred.resolve(Event); });

		return deferred.promise;
	};

	Dockets.getTicketVenue = function(ticketId) {
		var deferred = $q.defer();

		$http
			.get('/v1/ticket/' + ticketId + '/venue')
			.success(function(Venue){ deferred.resolve(Venue); });

		return deferred.promise;
	};

	return Dockets;

}]);
