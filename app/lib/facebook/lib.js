angular.module('vns.facebook', [])
	.factory('Facebook', ['$q', '$rootScope', function($q, $rootScope) {

		var resolve = function(errval, retval, deferred) {
			$rootScope.$apply(function() {
			  if (errval) {
			    deferred.reject(errval);
			  } else {
			    retval.connected = true;
			    deferred.resolve(retval);
			  }
			});
		};

		return function(FB)
		{
			var facebookService = {

				purchaseItem: function(product, quantity){

					var deferred = $q.defer();

					quantity = quantity || 1;

					FB.ui({
						method: 'pay',
						action: 'purchaseItem',
						product: product,
						quantity: quantity
					}, 
					function(response){
						deferred.resolve(response)
					});

					return deferred.promise;
				},

				postToFeed: function(config){

					var deferred = $q.defer();

					config.method = 'feed';
					FB.ui(config, function(response){
						deferred.resolve(response)
					});

					return deferred.promise;
				},

				logout: function(){
					var deferred = $q.defer();

					FB.logout(function(response){
						deferred.resolve();
					});

					return deferred.promise;
				},

				login: function(permissions){
					var deferred = $q.defer();

					var justLogin = function(){
						FB.login(function(response){
							if (response.authResponse)
			    				deferred.resolve();
							else
						  		deferred.reject(response.error);
						},{ scope: permissions.join() });
					};

					FB.getLoginStatus(function(response) {

						if (response.status == 'connected')
							facebookService
								.query('/me/permissions')
								.then(function(fbPermissions){

									var given = {};
									_.each(fbPermissions.data, function(fbPermission){
										given[fbPermission.permission] = fbPermission.status;
									});

									var unsatisfiedPermission =
										_.find(permissions, function(permission){
											return !given[permission] || given[permission] != 'granted';
										});
									
									if (unsatisfiedPermission)
										justLogin();
									else 
										deferred.resolve();
								});
						else
							justLogin();

					});

					return deferred.promise;	
				},

				getUser: function() {

					var deferred = $q.defer();

					FB.getLoginStatus(function(response) {

						if (response.status == 'connected')
							FB.api('/me', function(response) {
								resolve(null, response, deferred);
							});
						else
							FB.login(function(response) {
								if (response.authResponse)
						  			FB.api('/me', function(response) {
						    			resolve(null, response, deferred);
						  			});
								else
								  resolve(response.error, null, deferred);
							});
					});

					return deferred.promise;
				},

				query: function(query) {

					var deferred = $q.defer();

					FB.api(query, function(response){
						resolve(null, response, deferred);
					});

					return deferred.promise;
				}
			}; 

			return facebookService;
		}
	}]);
