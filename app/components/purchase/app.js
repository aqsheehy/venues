var App = angular.module('vns', ['ui.bootstrap', 'vns.facebook', 'vns.dockets']);

App.filter('date', function(){
	return function(value){
		var d = new Date(value),
	    minutes = d.getMinutes().toString().length == 1 ? '0'+d.getMinutes() : d.getMinutes(),
	    hours = d.getHours().toString().length == 1 ? '0'+d.getHours() : d.getHours(),
	    ampm = d.getHours() >= 12 ? 'pm' : 'am',
	    months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
	    days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
		return days[d.getDay()]+' '+months[d.getMonth()]+' '+d.getDate()+' '+d.getFullYear()+' '+hours+':'+minutes+ampm;
	};
});

App.controller('MainCtrl', ['$scope', '$http', 'Facebook', 'Dockets', function ($scope, $http, Facebook, Dockets){

	var eventId = 'fb/' + $scope.eventId;

	Facebook = Facebook(FB);

	Facebook
		.getUser()
		.then(function(FbUser){

			Dockets
				.getEvent(eventId)
				.then(function(Event){ $scope.Event = Event; });

			Dockets
				.getEventVenue(eventId)
				.then(function(Venue){ $scope.Venue = Venue; });

			Dockets
				.getEventTicket(eventId, FbUser.id)
				.then(function(TicketForEvent){ 
					$scope.TicketForEvent = TicketForEvent; 
					$scope.TicketUrl = Dockets.getTicketAttendanceUrl(TicketForEvent);
				});

			$scope.InitPurchase = function(){

				var productUrl = Dockets.getEventProductUrl('fb/' + $scope.Event.fbid);

				/* Continuously poll for the ticket until its found. */
				function TryGetTicketForEvent(){
					Dockets
						.getEventTicket(eventId, FbUser.id)
						.then(function(TicketForEvent){ 
							if (!TicketForEvent) return setTimeout(TryGetTicketForEvent, 1000);
							$scope.TicketForEvent = TicketForEvent;
							$scope.TicketUrl = Dockets.getTicketAttendanceUrl(TicketForEvent);
							$scope.WaitingForTicket = false;
						});
				};

				Facebook
					.purchaseItem(productUrl)
					.then(function(data) {
						$scope.WaitingForTicket = true;
						TryGetTicketForEvent();
					});
			};

			$scope.ShareEvent = function(){
				Facebook
					.postToFeed({
						link: 'https://' + Dockets.appHost + '/event/' + $scope.eventId,
						name: $scope.Event.title,
						caption: $scope.Venue.title,
						picture: 'https://cdn2.iconfinder.com/data/icons/windows-8-metro-style/26/ticket.png'
					});
			};

		});
}]);