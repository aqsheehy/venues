var App = angular.module('vns', ['ui.bootstrap', 'vns.facebook', 'vns.dockets']);

App.controller('MainCtrl', ['$scope', '$http', '$modal', 'Facebook', 'Dockets', function($scope, $http, $modal, Facebook, Dockets){

	$scope.Now = new Date().getTime();
	$scope.Events = {};

	Facebook = Facebook(FB);

	Facebook
		.login(['user_events'])
		.then(function(){
			Facebook
				.getUser()
				.then(function(User){
					$scope.User = User;
					LoadEventData();
				});
		});

	$scope.ShareEvent = function(ev){
		Facebook
			.postToFeed({
				link: 'https://' + Dockets.appHost + '/event/' + ev.FromFacebook.id,
				name: ev.FromFacebook.name,
				caption: $scope.Venue.title,
				picture: 'https://cdn2.iconfinder.com/data/icons/windows-8-metro-style/26/ticket.png'
			});
	};

	$scope.Monetize = function(ev)
	{
		$modal.open({
			templateUrl: '/components/manage/monetization.html',
			controller: 'PricingInstanceCtrl',
			resolve: {
				Event: function () { return ev.FromFacebook; },
				$http: function() { return $http; },
				Facebook: function(){ return Facebook; },
				Venue: function(){ return $scope.Venue; },
				Dockets: function(){ return Dockets; }
			}
		})
		.result
		.then(LoadEventData);
	};

	function LoadEventData(){

		Facebook
			.query('/me/events/created')
			.then(function(FacebookEvents){ 
				_.each(FacebookEvents.data, function(fbEvent){
					$scope.Events[fbEvent.id] = $scope.Events[fbEvent.id] || {};
					fbEvent.start = new Date(fbEvent.start_time).getTime();
					fbEvent.end = new Date(fbEvent.end_time).getTime();
					$scope.Events[fbEvent.id].FromFacebook = fbEvent;
				});
			});

		var venueId = 'fb/' + $scope.User.id;

		Dockets
			.getVenue(venueId)
			.then(function(Venue){ 
				$scope.Venue = Venue;	
			});

		Dockets
			.getVenueEvents(venueId)
			.then(function(VenueEvents){
				_.each(VenueEvents, function(venueEvent){
					$scope.Events[venueEvent.fbid] = $scope.Events[venueEvent.fbid] || {};
					$scope.Events[venueEvent.fbid].FromVenue = venueEvent;
				});
			});
	}
}]);

App.controller('PricingInstanceCtrl', ['$scope', '$modalInstance', '$http', 'Facebook', 'Event', 'Venue', 'Dockets', function($scope, $modalInstance, $http, Facebook, Event, Venue, Dockets){

	$scope.Cancel = function(){
		$modalInstance.dismiss('cancel');
	};

	$scope.Submit = function() {
		Dockets
			.venueHosts('fb/' + Venue.fbid, {
				fbid: Event.id,
				title: Event.name,
				description: Event.description || '',
				start: Event.start,
				end: Event.end,
				pricing: PricingFromCost($scope.Cost)
			})
			.then(function(){
				$modalInstance.close();
			});
	};

}]);

function PricingFromCost(cost)
{
	cost = Number(cost);

	return {
		cost: cost + (cost / 10),
		pretax: cost,
		price: (cost + (cost / 10)) + 0.5
	};
}