See. https://venues.atlassian.net/

Initial provision:

CREATE (v1:Venue)
SET v1.title = 'Jeremy Ingleson'
SET v1.fbid = '719759031450526'
CREATE (v2:Venue)
SET v2.title = 'Starlight Club'
SET v2.fbid = '1551296535084118'
CREATE (a:Address)
SET a.streetAddress = '6001/43 Herschel Street'
SET a.postcode = '4000'
SET a.country = 'Australia'
SET a.region = 'Queensland'
SET a.locality = 'Brisbane'
SET a.latitude = -27.452118
SET a.longitude = 153.031598
CREATE (v1)-[:located]->(a)
CREATE (v2)-[:located]->(a)

DELETE ALL::

MATCH (n)
OPTIONAL MATCH (n)-[r]-()
DELETE n,r


/* Test facebook accounts */

Starlight Club - 1551296535084118
Dominic Lucius
Jasper Stav


amount: "14.25"

currency: "USD"

payment_id:

quantity: "1"

signed_request: "hLabEN31CC4DSmXusgChthdMt7rnJRGLvhXjGury6cw.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImFtb3VudCI6IjE0LjI1IiwiY3VycmVuY3kiOiJVU0QiLCJpc3N1ZWRfYXQiOjE0MTUxOTY5NTcsInBheW1lbnRfaWQiOjU1OTMzMzM0MDg2MzczMCwicXVhbnRpdHkiOiIxIiwic3RhdHVzIjoiY29tcGxldGVkIn0"

status: "completed"