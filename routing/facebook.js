var paymentsVerifyToken = '4324234hj234kh234kjnfkj';

var _ = require('underscore'), 
	graph = require('../lib/localneo4j'),
	cqrs = require('../lib/cqrs');	

// Load handlers.
var tickets = require('../handlers/tickets'),
	events = require('../handlers/events'),
	facebook = require('../handlers/facebook');

function sendError(res, err) { res.status(500).send(err); }

module.exports = function(app){	

	app.post('/app/fb', function(req, res){
		res.send('This is awkward');
	});

	app.post('/app/fb/event/:id', function(req, res){
		res.render('purchase', { id: req.params.id });
	});

	app.post('/app/fb/ticket/:id', function(req, res) {
		res.render('ticket', { id: req.params.id });
	});

	app.post('/app/fb/manage', function(req, res){
		res.render('manage');
	});

	app.get('/app/fb/payment', function(req, res){
		if (req.query['hub.verify_token'] == paymentsVerifyToken)
			res.send(req.query['hub.challenge']);
		else
			res.status(500).send('OK');
	});

	app.post('/app/fb/payment', function(req, res){

		var paymentId = req.body.entry[0].id;
		var paymentApiPath = '/' + paymentId + '?access_token=' + facebook.appId + '|' + facebook.appSecret;

		cqrs.query(new facebook.v1.GraphQuery(paymentApiPath), function(err, purchaseSummary){
			if (err) return sendError(res, err);

			var productInformation = purchaseSummary.items[0].product.split('/').slice(-3);
			var productType = productInformation[0];
			var productAliasType = productInformation[1];
			var productId = productInformation[2];
			var userFbId = purchaseSummary.user.id;
			var purchaseTime = new Date(purchaseSummary.created_time).getTime();
			var ticketId = tickets.makeId();
			var ticketSecret = tickets.makeSecret();

			var ticketPurchasedCommand = new tickets.v1.TicketPurchased(productId, userFbId, purchaseTime, ticketId, ticketSecret);

			cqrs.call(ticketPurchasedCommand, function(err){
				if (err) sendError(res, err);

				var notificationApiPath = '/' + userFbId + '/notifications?access_token=' + facebook.appId + '|' + facebook.appSecret + '&template=Ticket%20successfully%20purchased&href=event%2F' + productId;

				cqrs.query(new facebook.v1.GraphCommand(notificationApiPath, null), function(err) {  
					if (err) return sendError(res, err);
					res.send('OK');
				});
			});
		});
	});

	app.get('/opengraph/event/fb/:id', function(req, res){
		cqrs.query(new events.v1.GetEvent(req.params.id), function(err, Event){
			if (err) return sendError(err);
			res.render('ogticket', { event: Event });
		});
	});

};