var paymentsVerifyToken = '4324234hj234kh234kjnfkj';

var _ = require('underscore'), 
	graph = require('../lib/localneo4j'),
	cqrs = require('../lib/cqrs');

// Load handlers.
var tickets = require('../handlers/tickets');
var events = require('../handlers/events');

function sendError(res, err) { res.status(500).send(err); }

module.exports = function(app){	

	app.get('/', function(req, res) { 
		res.send('This is awkward');
	});

	app.get('/event/fb/:id', function(req, res) {
		res.render('purchase', { id: req.params.id });
	});

	app.get('/manage', function(req, res){
		res.render('manage');
	});

	/*  
		Attendance API is meant to be as lightweight as possible.
		As a result, all orchestration occurs on the server side.
	*/
	app.get('/manage/ticket/:id/attend', function(req, res) {

		cqrs.query(new tickets.v1.GetTicket(req.params.id), function(err, ticket){
			if (err) return sendError(res, err);
			if (ticket.used) return res.render('fail', { previousAttempts: 1 });
			
			cqrs.call(new tickets.v1.TicketUsed(req.params.id, new Date().getTime()), function(err){
				if (err) return sendError(res, err);
				res.render('success');
			});
		});
	});
};
