var cqrs = require('../lib/cqrs');

// Load handlers.
var acts = require('../handlers/acts');
var venues = require('../handlers/venues');
var events = require('../handlers/events');
var tickets = require('../handlers/tickets');

function handleFor(res){
	return function(err, d){
		if (err) return res.status(500).send(err);
		res.json(d);
	};
}

function callFor(res, cmd){
	cqrs.call(cmd, handleFor(res));
}

function queryFor(res, cmd){
	cqrs.query(cmd, handleFor(res));	
}

module.exports = function(app){	

	app.get('/v1/act', function(req, res){
		res.json([]);
	});

	app.post('/v1/act/fb', function(req, res){
		callFor(res, new acts.v1.ActRegistered(req.body));
	});

	app.get('/v1/act/fb/:id', function(req, res){
		queryFor(res, new acts.v1.GetAct(req.params.id));
	});

	app.patch('/v1/act/fb/:id', function(req, res){
		req.body.fbid = req.params.id;
		callFor(res, new acts.v1.ActUpdated(req.body));
	});

	app.post('/v1/act/fb/:id/performance', function(req, res){
		req.body.fbid = req.params.id;
		callFor(res, new acts.v1.PerformingAt(req.body));
	});

	app.get('/v1/event', function(req, res){
		res.json([]);
	});

	app.get('/v1/event/fb/:id', function(req, res) {
		queryFor(res, new events.v1.GetEvent(req.params.id));
	});

	app.patch('/v1/event/fb/:id', function(req, res){
		req.body.fbid = req.params.id;
		callFor(res, new events.v1.EventUpdated(req.body));
	});

	app.get('/v1/event/fb/:id/venue', function(req, res){
		callFor(res, new events.v1.GetEventVenue(req.params.id));
	});

	app.get('/v1/event/fb/:id/acts', function(req, res){
		callFor(res, new events.v1.GetEventActs(req.params.id));
	});

	app.get('/v1/event/fb/:id/ticket', function(req, res){
		queryFor(res, new events.v1.GetEventTicket(req.params.id, req.query.purchaser));
	});

	app.get('/v1/venue', function(req, res){
		res.json([]);
	});

	app.post('/v1/venue/fb', function(req, res){
		callFor(res, new venues.v1.VenueRegistered(req.body));
	});

	app.get('/v1/venue/fb/:id', function(req, res){
		queryFor(res, new venues.v1.GetVenue(req.params.id));
	});

	app.patch('/v1/venue/fb/:id', function(req, res) {
		req.body.fbid = req.params.id;
		callFor(res, new venues.v1.VenueUpdated(req.body));
	});

	app.post('/v1/venue/fb/:id/event', function(req, res) {
		req.body.fbid = req.params.id;
		callFor(res, new venues.v1.VenueHosts(req.body));
	});

	app.get('/v1/venue/fb/:id/event', function(req, res){
		callFor(res, new venues.v1.GetVenueEvents(req.params.id));
	});

	app.post('/v1/event/fb/:id/purchase', function(req, res){
		// Generate the identifiers outside the scope of CQRS.
		// IDs must be statically defined so as to allow for re-execution.
		var prms = {
			eventfbid: req.params.id, 
			personfbid: req.body.personfbid, 
			purchaseTime: new Date().getTime(), 
			ticketId: tickets.makeId(), 
			ticketSecret: tickets.makeSecret()
		};
		callFor(res, new tickets.v1.TicketPurchased(prms));
	});

	app.get('/v1/ticket/:id', function(req, res){
		queryFor(res, new tickets.v1.GetTicket(req.params.id));
	});

	app.post('/v1/ticket/:id/attended', function(req, res){
		callFor(res, new tickets.v1.EventAttended(req.params.id));
	});

	app.get('/v1/ticket/:id/event', function(req, res){
		queryFor(res, new tickets.v1.GetTicketEvent(req.params.id));
	});

	app.get('/v1/ticket/:id/venue', function(req, res){
		queryFor(res, new tickets.v1.GetTicketVenue(req.params.id));
	});
};