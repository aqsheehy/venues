module.exports = {
	make: function (number, type)
	{
	    var text = [];
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	    if (type)
	    	text.push(type + '-');

	    for( var i=0; i < number; i++)
	        text.push(possible.charAt(Math.floor(Math.random() * possible.length)));

	    return text.join('');
	}
};