var redis = require('./localredis').client;

var handlers = {};

module.exports = {
	module: GenerateModule,
	handle: AddHandler,
	call: function(nameOrData, data, fn) { return Execute(nameOrData, data, fn, true); },
	query: function(nameOrData, data, fn) { return Execute(nameOrData, data, fn, false); }
};

function AddHandler(version, name, action){
	if (!handlers[version])
		handlers[version] = {};
	handlers[version][name] = action;
}

function SaveCommand(type, command){
	redis.zadd([type, new Date().getTime(), command]);
}

function Execute(nameOrData, data, fn, save){

	// If the name must be inferred, shift the parameters left.
	if (typeof nameOrData == typeof {})
	{
		fn = data;
		data = nameOrData;
		nameOrData = nameOrData.constructor.name;
	}

	if (save && redis) SaveCommand(nameOrData, data);

	var handler = (handlers[data.$commandVersion] || {})[nameOrData];

	if (handler) return handler(data, fn);

	fn('Handler with of type ' + nameOrData + ' of version ' + data.$commandVersion + ' not defined.');
}

function GenerateModule(version, definitions){

	if (!definitions)
	{
		definitions = version;
		version = "v1";
	}

	var packageCommands = {};
	for (var i = 0; i < definitions.length; i++)
	{
		var definition = definitions[i];
		var command = definition[0];
		command.prototype.$commandVersion = version;
		var commandType = command.prototype.constructor.name;
		var handler = definition[1];
		packageCommands[commandType] = command;
		AddHandler(version, commandType, handler);
	}
	return packageCommands;
	
}