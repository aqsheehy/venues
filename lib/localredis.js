if (process.env.REDISTOGO_URL)
{
	var rtg = require("url").parse(process.env.REDISTOGO_URL);
	redis = require("redis").createClient(rtg.port, rtg.hostname);
 	redis.auth(rtg.auth.split(":")[1]);
 	exports.module = {
 		client: redis
 	};
}
else 
	exports.module = { 
		client: null
	};

