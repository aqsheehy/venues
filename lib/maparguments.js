var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
var ARGUMENT_NAMES = /([^\s,]+)/g;

function getParamNames(func) {
  var fnStr = func.toString().replace(STRIP_COMMENTS, '')
  var result = fnStr.slice(fnStr.indexOf('(')+1, fnStr.indexOf(')')).match(ARGUMENT_NAMES)
  if(result === null)
     result = []
  return result
}

module.exports = function(ref, arguments){
	var params = getParamNames(ref.constructor);
	var args = Array.prototype.slice.call(arguments);

	// If the only given parameter is an object, assume its a configuration object.
	if (args.length == 1 && 
		args.length != params.length && 
		typeof args[0] == typeof {})
	{
		var config = args[0];
		for (var i = 0; i < params.length; i++)
		{
			ref[params[i]] = config[params[i]];
		}
	}
	else
	{
		for (var i = 0; i < args.length; i++)
		{
			ref[params[i]] = args[i];
		}	
	}
};