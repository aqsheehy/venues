var neo4j = require('neo4j-js');
var _ = require("underscore");

var baseNeo4jUrl = 
	process.env['NEO4J_URL'] || 
	process.env['GRAPHENEDB_URL'] || 
	'http://localhost:7474';

var neo4jUrl = 
	baseNeo4jUrl + '/db/data/';

var query = function(str, p, fn) {
	neo4j.connect(neo4jUrl, function(err, graph){
		if (err)
			fn(err);
		else
			graph.query(str, p, fn);
	});
};

var stringFormat = /{-?(([a-zA-Z]){0,50})}/g;

function ExtractParameters(format, prm) {
	var names = format.match(stringFormat);
	var corrected = _.map(names, function(name){
		return name.substring(1, name.length - 1);
	});
	var extracted = {};
	_.each(corrected, function(name){
		extracted[name] = prm[name];
	});
	return extracted;
}

function QueryBuilder() {
	delete this.queryString;
	this.parameters = {};

	this.line = function(type, str, prm){
		if (this.queryString) this.queryString = this.queryString + '\n';
		this.queryString = (this.queryString || '') + type + ' ' + str;
		if (prm) {
			var extractedPrms = ExtractParameters(str, prm);
			for (var i in extractedPrms) 
				this.parameters[i] = extractedPrms[i]
		};
		return this;
	}

	this.create = function(str, prm){
		return this.line('CREATE', str, prm);
	}

	this.match = function(str, prm){
		return this.line('MATCH', str, prm);
	};

	this.optionalMatch = function(str, prm){
		return this.line('OPTIONAL MATCH', str, prm);
	};

	this.merge = function(str, prm){
		return this.line('MERGE', str, prm);
	};

	this.where = function(str, prm){
		return this.line('WHERE', str, prm);
	};

	this.and = function(str, prm) {
		return this.line(' AND', str, prm);
	};

	this.set = function(str, prm){	
		return this.line('SET', str, prm);
	};

	this.returns = function(str, fn){
		this.line('RETURN', str);
		if (fn) this.execute(fn);
		return this;
	};

	this.params = function(prm){
		for (var i in prm) this.parameters[i] = prm[i];
		return this;
	};

	this.param = function(name, prm){
		this.params[name] = prm;
		return this;
	}

	this.execute = function(fn){
		query(this.queryString, this.parameters, fn);
	};
}

module.exports = {
	query: query,
	queryable: function(){
		return new QueryBuilder();
	}
};