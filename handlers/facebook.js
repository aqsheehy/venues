var appId = 856946490995122;
var appSecret = '7d1a56dfcdd72b9c1d1c6f7c10801cd0';

var cqrs = require('../lib/cqrs'),
	https = require('https'),
	map = require('../lib/maparguments');

function makeGraphRequest(method, apiPath, data, callback) {

	var options = {
        host: 'graph.facebook.com',
        port: 443,
        path: apiPath,
        method: method
    };

    var buffer = '';

    var request = https.request(options, function(result){
        result.setEncoding('utf8');

        result.on('data', function(chunk){
            buffer += chunk;
        });
        
        result.on('end', function(){
        	callback(null, buffer);
        });
    });

    if (data)
    	request.write(post_data);

    
    request.on('error', function(e){
        callback(e);
    });

    request.end();

}

var v1 = cqrs.module("v1", [

	/*
		Executes something against the facebook graph.
		@param	apiPath			The command against the facebook graph to be executed.
	*/
	[function GraphCommand(apiPath, values){ map(this, arguments); },
		function(command, callback){
			makeGraphRequest('POST', command.apiPath, command.values, function(err, buffer){
				var finalResult = JSON.parse(buffer);
	        	if (finalResult.error)
	        		callback(finalResult.error);
	        	else
	            	callback(null, finalResult);
			});
		}],

	/*
		Makes a query out to the facebook graph.
		@param	apiPath			The query against the facebook graph to be executed.
	*/
	[function GraphQuery(apiPath){ map(this, arguments); },
		function (command, callback){
			makeGraphRequest('GET', command.apiPath, null, function(err, buffer){
				var finalResult = JSON.parse(buffer);
	        	if (finalResult.error)
	        		callback(finalResult.error);
	        	else
	            	callback(null, finalResult);
			});
		}],

	/* 
		Gets the access token for the app.
	*/
	[function GetAccessToken(){ map(this, arguments); },
		function(command, callback){
			var path = '/oauth/access_token?client_id=' + appId + '&client_secret=' + appSecret + '&grant_type=client_credentials';
			makeGraphRequest('GET', path, null, callback);
		}]

]);

module.exports = { 
	v1: v1,
	appId: appId,
	appSecret: appSecret
};