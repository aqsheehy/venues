var _ = require('underscore'),
	cqrs = require('../lib/cqrs'),
	map = require('../lib/maparguments'),
	graph = require('../lib/localneo4j'),
	ids = require('../lib/ids');

var v1 = cqrs.module("v1", [

	/* 
		Returns the details about a venue.
		@param	fbid	The facebook identifier of the venue.
	*/
	[function GetVenue(fbid) { map(this, arguments); },
		function(command, fn) {
		    graph.queryable()
		    	.match('(venue:Venue)-[:located]->(addr:Address)')
		    	.where('venue.fbid = {fbid}', command)
		    	.returns('venue,addr', function(err, results){
			    	if (err) return fn(err);
			    	
		    		var result = results[0];
		    		var venue = {};

		    		for (var i in result.venue.data)
		    			venue[i] = result.venue.data[i];

		    		venue.address = result.addr.data;

		    		fn(null, venue);
			    });
		}],

	/* 
		Returns a list of the events hosted by a venue.
		@param	fbid	The facebook identifier of the venue.
	*/
	[function GetVenueEvents(fbid){ map(this, arguments); },
		function(command, fn){
			graph.queryable()
				.match('(venue:Venue)-[:hosts]->(event:Event)')
				.where('venue.fbid = {fbid}', command)
				.returns('event', function(err, results){
					if (err) return fn(err);
					fn(null, _.map(results, function(result){
						return _.extend({}, result.event.data);
					}));
				});
		}],

	/* 
		When a new venue is to be registered to the system. 
		@param	fbid	The facebook identifier of the venue.
		@param	title	The name of the venue, probably the same as the facebook page.
		@param	description		Description of the venue if any.
		@param	address Address information of the venue (country, locality, region etc.)
	*/
	[function VenueRegistered(fbid, title, description, address) { map(this, arguments); },
		function(command, fn){
		    graph.queryable()
		    	.create('(venue:Venue {title:{title}, description:{description}})', command)
		    	.create('(addr:Address {country:{country}, locality:{locality}, region:{region}, postcode: {postcode}, streetAddress: {streetAddress}, latitude: {latitude}, longitude: {longitude}})', command.address)
		    	.create('(venue)-[:located]->(addr)')
		    	.execute(fn);
		}], 

	/* 
		When a venue hosts a new event; registers a new event to the system.
		@param	fbid	The facebook identifier of the venue.
		@param	eventfbid	The facebook identifier of the event.
		@param 	title 	The name of the event, probably the same as the facebook page.
		@param 	description The description of the event if any.
		@param	start	The unix timestamp of the start time (UTC)
		@param	end		The unix timestamp of the end time (UTC) if any.
		@param	pricing	Pricing information of the event.
	*/
	[function VenueHosts(fbid, eventfbid, title, description, start, end, pricing) { map(this, arguments); },
		function(command, fn) {
			graph.queryable()
				.match('(venue:Venue {fbid: {fbid}} )', command)
				.merge('(event:Event { fbid: {eventfbid} })', command)
				.set('event.title = {title}', command)
				.set('event.description = {description}', command)
				.set('event.start = {start}', command)
				.set('event.end = {end}', command)
				.set('event.price = {price}', command.pricing)
				.set('event.cost = {cost}', command.pricing)
				.set('event.pretax = {pretax}', command.pricing)
				.create('(venue)-[:hosts]->(event)')
		    	.execute(fn);
		}], 

	/* 
		When the details of the venue have been updated. 
		@param	fbid	The facebook identifier of the venue.
		@param	description The description of the event if any.
		@param	address Address information of the venue (country, locality, region etc.)
	*/
	[function VenueUpdated(fbid, description, address){ _$(this, arguments); },
		function(command, fn){
		    graph.queryable()
		    	.match('(venue:Venue)-[:located]->(addr:Address)')
		    	.where('venue.fbid = {fbid}', command)
		    	.set('venue.description = {description}', command)
		    	.set('addr.country = {country}', command.address)
		    	.set('addr.region = {region}', command.address)
		    	.set('addr.postcode = {postcode}', command.address)
		    	.set('addr.locality = {locality}', command.address)
		    	.set('addr.streetAddress = {streetAddress}', command.address)
		    	.set('addr.latitude = {latitude}', command.address)
		    	.set('addr.longitude = {longitude}', command.address)
		    	.execute(fn);
		}]
]);

module.exports = { 
	v1: v1,
	makeId: function(){
		return ids.make(20, 'vn');
	}
};