var cqrs = require('../lib/cqrs'),
	map = require('../lib/maparguments'),
	graph = require('../lib/localneo4j'),
	ids = require('../lib/ids');

var v1 = cqrs.module("v1", [

	/*
		Registers a new band to the domain.

		@param	fbid	The facebook identifier of the act (facebook account)
		@param	title	The name of the act. This will usually be the facebook name.
		@param	description	Description if any of the act.
	*/
	[function ActRegistered(fbid, title, description){ map(this, arguments); },
		function(command, fn){
		    graph.queryable()
		    	.merge('(act:Act { fbid: {fbid} })', command)
		    	.set('act.title = {title}', command)
		    	.set('act.description = {description}', command)
		    	.execute(fn);
		}],

	/*
		Denotes that the act with a given actfbid will be performing at an event with the fbid provided.

		@param	actfbid	The facebook identifier of the act.
		@param	fbid	The facebook identifier of the event.
	*/
	[function PerformingAt(fbid, eventfbid){ 
		map(this, arguments); 
	},
		function(command, fn){
		    graph.queryable()
		    	.merge('(event:Event{ fbid: {eventfbid} })', command)
		    	.merge('(act:Act{ fbid: {fbid} })', command)		    	
	    		.create('(event)-[:features]->(p:Performance)-[:by]->(act)')
	    		.execute(fn);
		}],

	/* 
		Returns the stored data about an act.

		@param	fbid	The facebook identifier of the act.
	*/
	[function GetAct(fbid){ map(this, arguments); },
		function(command, fn){
		    graph.queryable()
		    	.match('(act:Act { fbid: {fbid} } )', command)
		    	.returns('act', function(err, results){
		    		if (err) return fn(err);
		    		fn(null, results[0].act.data);
			    });
		}],

	/*
		Updates the core details about an act.

		@param	fbid	The facebook identifier of the act.
		@param	description The updated description of the act.
	*/
	[function ActUpdated(fbid, description){ map(this, arguments); },
		function(command, fn){
		    graph.queryable()
		    	.match('(act:Act { fbid: {fbid} } )', command)
		    	.set('description = {description}', command)
		    	.execute(fn);
		}]

]);

module.exports = { 
	v1: v1,
	makeId: function(){
		return ids.make(20, 'ac')
	}
};