var cqrs = require('../lib/cqrs'),
	map = require('../lib/maparguments'),
	graph = require('../lib/localneo4j'),
	ids = require('../lib/ids');

var v1 = cqrs.module("v1", [

	/* 
		Gets the ticket for a given id (e.g. tk-123456789...)
		@param	ticketId	The unique identifier of the ticket.
	*/	
	[function GetTicket(ticketId){ map(this, arguments); },
		function(command, fn){
		    graph.queryable()
		    	.match('(ticket:Ticket)')
		    	.where('ticket.ticketId = {ticketId}', command)
		    	.returns('ticket', function(err, results){
			    	if (err) return fn(err);
			    	fn(null, results[0].ticket.data);
			    });
		}],

	/* 
		Gets the venue hosting the event that this ticket was for.
		@param	ticketId	The unique identifier of the ticket.
	*/
	[function GetTicketVenue(ticketId){ map(this, arguments); },
		function(command, fn){
			graph.queryable()
				.match('(ticket:Ticket)-[:for]->(event:Event)<-[:hosts]-(venue:Venue)')
				.where('ticket.ticketId = {ticketId}', command)
				.returns('venue', function(err,results){
					if (err) return fn(err);
					fn(null, results[0].venue.data);
				});
		}],

	/*
		Gets the event the ticket was for.
		@param	ticketId	The unique identifier of the ticket.
	*/
	[function GetTicketEvent(ticketId){ map(this, arguments); },
		function(command, fn){
			graph.queryable()
				.match('(ticket:Ticket)-[:for]->(event:Event)<-[:hosts]-(venue:Venue)')
				.where('ticket.ticketId = {ticketId}', command)
				.returns('event', function(err,results){
					if (err) return fn(err);
					fn(null, results[0].event.data);
				});
		}],

	/* 
		Gets the attendance count for a given ticket.
		@param	ticketId	The unique identifier of the ticket.
	*/
	[function GetTicketAttendanceCount(ticketId){ map(this, arguments); }, 
		function(command, fn){
			graph.queryable()
				.match('(person:Person)-[:purchased]->(ticket:Ticket{ ticketId: {ticketId} })-[:for]->(event:Event)', command)
				.match('(event)<-[a:attended]-(person)')
				.returns('a', function(err, results){
					if (err) return fn(err);
					return fn(null, results.length);
				});
		}],

	/* 
		Registers a new ticket to the event gives access to an individual.
		@param	eventfbid	The facebook identifier of the event of which the ticket was purchased.
		@param	personfbid	The facebook identifier of the person who bought the ticket.
		@param	purchaseTime	The unix timestamp of the time in which the ticket was purchased.
		@param	ticketId	The unique identifier of the ticket purchased (defined prior to creation).
		@param	ticketSecret	Secret available to ticket owners, so as to prevent fraud.
	*/
	[function TicketPurchased(eventfbid, personfbid, purchaseTime, ticketId, ticketSecret){ map(this, arguments); },
		function(command, fn){
		    graph.queryable()
		    	.match('(event:Event{fbid: {eventfbid}})', command)
		    	.merge('(person:Person{fbid: {personfbid}})', command)
	    		.create('(person)-[:purchased]->(ticket:Ticket)-[:for]->(event)')
	    		.set('ticket.ticketId = {ticketId}', command)
	    		.set('ticket.purchaseTime = {purchaseTime}', command)
	    		.set('ticket.ticketSecret = {ticketSecret}', command)
	    		.execute(fn);
		}],

	/* 
		Denotes an individuals attendance to an event. 
		@param	ticketId	The unique identifier of the ticket that was used up (event attended).
	*/
	[function EventAttended(ticketId) { map(this, arguments); },
		function(command, fn){
		    graph.queryable()
		    	.match('(person:Person)-[:purchased]->(ticket:Ticket)-[:for]->(event:Event)')
		    	.where('ticket.ticketId = {ticketId}', command)
	    		.create('(event)<-[:attended]-(person)')
	    		.execute(fn);
		}],

	/* 
		Denotes that a ticket has been used once.
		@param	ticketId	The unique identifier of the ticket that was used up.
		@param	usedDate	The timestamp on which the ticket was used.
	*/
	[function TicketUsed(ticketId, usedDate) { map(this, arguments); },
		function(command, fn){
			graph.queryable()
				.match('(ticket:Ticket{ ticketId: {ticketId} })', command)
				.set('ticket.used = {usedDate}', command)
				.execute(fn);
		}]
]);

module.exports = { 
	v1: v1,
	makeId: function(){
		return ids.make(20, 'tk');
	},
	makeSecret: function(){
		return ids.make(30);
	}
};