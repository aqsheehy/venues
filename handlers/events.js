var _ = require('underscore'),
	cqrs = require('../lib/cqrs'),
	map = require('../lib/maparguments'),
	graph = require('../lib/localneo4j'),
	ids = require('../lib/ids');

var	v1 = cqrs.module("v1", [

		/*
			Returns the details about an event. 
			@param	fbid	The facebook identifier of the event.
		*/
		[function GetEvent(fbid) { map(this, arguments); },
			function(command, fn){
			    graph.queryable()
			    	.match('(event:Event{ fbid: {fbid} })', command)
			    	.returns('event', function(err, results){
				    	if (err) return fn(err);
				    	fn(null, results[0]['event'].data);
				    });
			}],

		/* 
			Returns the details of the venue hosting a given event.
			@param	fbid	The facebook identifier of the event.
		*/
		[function GetEventVenue(fbid){ map(this, arguments); },
			function(command, fn){
				graph.queryable()
					.match('(event:Event)<-[:hosts]-(venue:Venue)-[:located]->(address:Address)')
					.where('event.fbid = {fbid}', command)
					.returns('address,venue', function(err, results){
						if (err) return fn(err);
						fn(null, _.extend({}, results[0].venue.data, results[0].address.data));
					});
			}],

		/* 
			Lists the acts that wil be performing in a given event.
			@param	fbid	The facebook identifier of the event.
		*/
		[function GetEventActs(fbid){ map(this, arguments); },
			function(command, fn){
				graph.queryable()
					.match('(event:Event)-[:features]->(p:Performance)-[:by]->(act:Act)')
					.where('event.fbid = {fbid}', command)
					.returns('act', function(err, results){
						if (err) return fn(err);
						fn(null, _.map(results, function(result){
							return _.extend({}, result.act.data);
						}));
					});
			}],

		/* 
			Updates the details of an event.			
			@param	fbid	The facebook identifier of the event.
			@param	description	The description if any of the event.
			@param	start	The unix timestamp for the start time (UTC).
			@param	end	 	The unix timestamp for the end time (UTC) if any.
			@param	pricing	Pricing information about the event (price, cost and pretax price)
		*/
		[function EventUpdated(fbid, description, start, end, pricing){ map(this, arguments); },
			function(command, fn){
			    graph.queryable()
			    	.match('(event:Event{fbid: {fbid}})', command)
			    	.set('event.description = {description}', command)
			    	.set('event.start = {start}', command)
			    	.set('event.end = {end}', command)
			    	.set('event.price = {price}', command.pricing)
			    	.set('event.cost = {cost}', command.pricing)
			    	.set('event.pretax = {pretax}', command.pricing)
			    	.execute(fn);
			}],

		/* 
			Gets the ticket for a given event and purchaser.
			@param	fbid	The facebook identifier of the event.
			@param	purchaser	The facebook identifier of the person that bought the ticket.
		*/
		[function GetEventTicket(fbid, purchaser){ map(this, arguments); },
			function(command, fn){
				graph.queryable()
					.match('(person:Person{fbid:{purchaser}})-[:purchased]->(ticket:Ticket)-[:for]->(event:Event{fbid: {fbid}})', command)
					.returns('ticket', function(err, results){
						if (err) return fn(err);
						if (!results || !results[0]) return fn();
						fn(null, _.extend({}, results[0].ticket.data));
					});
			}]
	]);

module.exports = { 
	v1: v1,
	makeId: function(){
		return ids.make(20, 'ev');
	}
};